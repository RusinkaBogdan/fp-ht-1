const isItemValid = ({ name, price, date }) => {
  return name && price && date;
};

const addDollar = order => {
  return {
    ...order,
    price: "$".concat(order.price)
  };
};

const capitalize = order => {
  return {
    ...order,
    name: _.capitalize(order.name)
  };
};

const createOrderString = order => `${order.name} - ${order.price}`;

const transformObject = order => {
  return _.flow(
    capitalize,
    addDollar,
    createOrderString
  )(order);
};

const groupOrdersByDate = orders => {
  const res = {};

  orders.reduce((acc, item) => {
    const orderString = transformObject(item);
    acc[item.date]
      ? acc[item.date].push(orderString)
      : (acc[item.date] = [orderString]);
    return acc;
  }, res);

  return res;
};

const getGroupedDataValues = res => {
  return _.values(res);
};

const alignDataArrays = data => {
  return _.map(data, n => {
    return [...n, ..._.times(2 - n.length, _.constant(null))];
  });
};

const rotateRowsToColumns = alignedArray => {
  return alignedArray[0].map(function(col, i) {
    return alignedArray.map(function(row) {
      return row[i];
    });
  });
};

const filterInvalidOrders = orders => orders.filter(item => !isItemValid(item));
const filterValidOrders = orders => orders.filter(item => isItemValid(item));
const stringifyResults = orders =>
  orders.map(order => stringifyOrderObject(order));
const stringifyOrderObject = order => JSON.stringify(order);

const getInvalidOrders = orders => {
  return _.flow(
    filterInvalidOrders,
    stringifyResults
  )(orders);
};

const getkeys = orders => {
  return _.keys(orders);
};

const getValidOrders = orders => {
  const data = _.flow(
    filterValidOrders,
    groupOrdersByDate,
    getGroupedDataValues,
    alignDataArrays,
    rotateRowsToColumns
  )(orders);

  const header = _.flow(
    filterValidOrders,
    groupOrdersByDate,
    getkeys
  )(orders);

  return {
    data,
    header
  };
};

const printValidOrders = orders => {
  _.flow(
    getValidOrders,
    renderOrders
  )(orders);

  return orders;
};

const printInvalidOrders = orders => {
  _.flow(
    getInvalidOrders,
    renderInvalidOrders
  )(orders);

  return orders;
};

function main() {
  _.flow(
    printValidOrders,
    printInvalidOrders
  )(orders);
}

document.addEventListener("DOMContentLoaded", main);
