const getOrdersList = () => document.getElementById("orders-list");
const getValidOrdersContainer = () => document.getElementById("correct");

const createTable = () => document.createElement("table");
const createTableBody = () => document.createElement("tbody");
const createTableRow = () => document.createElement("tr");
const createHeaderCell = () => document.createElement("th");
const createTableCell = () => document.createElement("td");
const createDiv = () => document.createElement("div");

const renderHeader = data => {
  const row = createTableRow();
  data.forEach(cell => {
    const th = createHeaderCell();
    th.innerHTML = cell;
    row.append(th);
  });
  return row;
};

const renderTableRow = data => {
  const row = createTableRow();
  data.forEach(cell => {
    const td = createTableCell();
    td.innerHTML = cell;
    row.appendChild(td);
  });
  return row;
};

const renderTables = items => {
  const list = getOrdersList();
  const div = createDiv();
  items.forEach(item => list.append(div));
};

const renderBody = list => {
  const tbody = createTableBody();

  list.map(order => {
    const row = renderTableRow(order);
    tbody.appendChild(row);
  });

  return tbody;
};

const renderOrders = ({ data, header }) => {
  const validOrdersContainer = getValidOrdersContainer();
  const table = createTable();
  validOrdersContainer.append(table);
  table.append(renderHeader(header));
  table.append(renderBody(data));
};

const renderInvalidOrder = container => {
  return order => {
    const orderWrapper = createDiv();
    orderWrapper.innerHTML = stringifyOrderObject(order);
    container.append(orderWrapper);
  };
};

const renderInvalidOrders = orders => {
  const container = getOrdersList();
  orders.map(renderInvalidOrder(container));
};
