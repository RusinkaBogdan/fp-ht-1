const printTable = data => console.log(data);

const flattenTable = table => _.flatten(table);

const replaceEqualWithUndefined = cell => item =>
  !_.isEqual(cell, item) ? item : null;

const replaceCellWithUndefined = cell => arr =>
  arr.map(replaceEqualWithUndefined(cell));

const insertItemToNewPosition = (i, j, item) => arr => {
  arr.splice(j * 3 + i + 1, 0, item);
  return arr;
};

const removeNull = array => _.compact(array);

const recalculatePositions = array =>
  array.map((item, i) => ({
    id: item.id,
    column: Math.floor((i / 3) % 3),
    row: i % 3,
    position: i
  }));

const transformToMatrix = array => _.chunk(3, array);

const rearrangeMatrix = (table, newColumn, newRow, item) =>
  _.pipe(
    flattenTable,
    replaceCellWithUndefined(item),
    insertItemToNewPosition(newColumn, newRow, item),
    removeNull,
    recalculatePositions,
    transformToMatrix
  )(table);

// Hometask
const table = [
  [
    { id: 0, row: 0, column: 0, position: 1 },
    { id: 1, row: 1, column: 0, position: 2 },
    { id: 2, row: 2, column: 0, position: 3 }
  ],
  [
    { id: 3, row: 0, column: 1, position: 4 },
    { id: 4, row: 1, column: 1, position: 5 },
    { id: 5, row: 2, column: 1, position: 6 }
  ],
  [
    { id: 6, row: 0, column: 2, position: 7 },
    { id: 7, row: 1, column: 2, position: 8 },
    { id: 8, row: 2, column: 2, position: 9 }
  ]
];

printTable(table);

const newColumn = 1;
const newRow = 1;
const item = table[0][0];

const newTable = rearrangeMatrix(table, newColumn, newRow, item);

printTable(newTable);
